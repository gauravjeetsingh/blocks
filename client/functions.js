fetchLayouts = function(theme){
	Meteor.call('getLayouts', theme, function(error, data){
			layoutArray = data;
			useLayouts(theme, layoutArray);
		});
}

viewPresentation = function(result){
	slides = result.presentation;
	renderJSON(slides, "calmness");
}

useLayouts = function(theme, layoutArray){
	layouts = layoutArray;
	themeName = theme;
	customAccordion();
}

addTextFields = function(tags, parentDiv){
	div = "#"+parentDiv.id +" input[type=text]";
	if($(div).length > 1) { $(div).remove(); }
	for(var i in tags){
		$(parentDiv).append("<input type='text' name='"+tags[i]+"' placeholder='"+tags[i]+"'>");
	}
	$('.accordion').accordion("refresh");
}

addNewSlide = function(count){
	$('#slideData').append("<div class=draggable>"+
					"<img class=\"sort-icon icon\" src=\"/icons/sort.svg\">"+
					"<img class=\"delete-icon icon\" src=\"/icons/dustbin.png\">"+
					"<h3 contenteditable=true class='slide-title'>"+
					"Untitled Slide"+"</h3>"+
					"<div class=pane id=slide"+count+"></div>"+
				"</div>");
	slideID ="#slide"+count;
	for(var i in layouts){
		$(slideID).append("<label>"+
		"<input type='radio' id=layout-"+count+" name='layout-"+count+"'"+
		"value='"+layouts[i]+"' class=layout>"+
		layouts[i]+"</label>");
	}
	$('.accordion').accordion("refresh");
}

addExistingSlides = function(){
	json = PresentationList.findOne();
	slides = json.presentation.slides;
	$('.editor-title').text(json.presentationTitle);
	for(var i in slides){
		addNewSlide(i);
		$('#layout-'+i+'[value='+slides[i].layout+']').trigger('change').attr('checked','checked');
		$.each(slides[i], function(key, value){
			$('input[type=text][name='+key+']').val(value);
		});
	}
}

deleteSlide = function(slide){
	console.log(slide);
	slide.remove();
}

refresh = function(){
	jsonObject = generateJSON('.accordion');
	renderJSON(jsonObject, themeName);
	if($('.editor-title').text() == "Untitled"){
		alert('Unable to save!! Please enter name of your presentation');
		$('.editor-title').focus();
	} else { save(); }
}

save = function(){
	var title = $('.editor-title').text();
	search= PresentationList.find({
			presentationTitle: title,
			userId: Meteor.userId()
		    });
	if(search.fetch()[0]){
		PresentationList.update(
		{ _id: search.fetch()[0]._id },{
			userId: Meteor.userId(),
			presentation:jsonObject,
			presentationTitle: title,
			modifiedAt: new Date()
		});
	} else{
		PresentationList.insert({
			userId: Meteor.user()._id,
			presentation: jsonObject,
			presentationTitle: title,
			modifiedAt: new Date()
		});
	}
}

customAccordion = function(){
	$(".accordion").accordion({
		header: "> div > h3",
		collapsible: true,
		animate:400
	})
	.sortable({
		axis:"y",
		handle:"img",
        	stop: function( event, ui ) {
			ui.item.children( "h3" ).triggerHandler( "focusout" );
			$( this ).accordion( "refresh" );
			refresh();
        	}
	});
}

generateJSON = function(container){
	slidesObj = {}
	jsonObj = [];

	var divs = $(container).find('div')

	for(i=0; i<divs.length; i++){
	if(divs[i].className == "draggable"){
		continue;
	}else{
	item = {}
		$("#"+divs[i].id+" input").each(function(){
			if(this.type == "radio"){
				var key = "layout";
				if(this.checked){ 
					var value = $(this).val(); 
					item [key] = value;
				}	
			} else{
				var key = $(this).attr("name");
				var value = $(this).val();
				if(this.name == "listItems"){
					value = value.split(',');
				}

				item [key] = value;
			}
		});
		jsonObj.push(item);
	}
	}

	slidesObj ["slides"] = jsonObj;

	return slidesObj;
}
