Template.form.events({
	'change .layout': function(e){
		var checked = e.currentTarget.defaultValue;
		var parentDiv = e.target.parentElement.parentElement;
		
		Meteor.call('getTemplate', checked, function(err, data){
						tags = getTemplateVariables(data);
						addTextFields(tags, parentDiv);
		});
	},

	'change input[name=bgImageTag]': function(e){
		temp = e.target;
		tag = e.target.value;
		imageUrl = getImage(tag);
		$(e.target.nextElementSibling).val(imageUrl);
	},

	'click .plus-icon': function(e){
		count = ++slide;
		addNewSlide(count);
	},

	'click .delete-icon': function(e){
		deleteSlide(e.target.parentElement);
		refresh();
	},

	'change #slideform': function(e){
		refresh();
	},

	'click .menu-icon': function(e){
		$('#slideform').slideToggle();
	},

	'click .slide-title':function(e){
		var active = $( ".accordion" ).accordion( "option", "active" );
		goToSlide(active);
	}
});

Template.form.onRendered(function() {
	slide = 0;
	$('.editor-title').attr('contenteditable', 'true');
	$('.editor-title').focus();
	$('.editor-title').text("Untitled");
});
