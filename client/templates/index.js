Template.index.events({
	'click .btn-github': function(){
		Meteor.loginWithGithub({
		  requestPermissions: ['email']
		  }, function (err) {
		  if (err)
		    Session.set('errorMessage', err.reason || 'Unknown error');
		  else
		     Router.go('form');
		console.log(err.reason);
		});
	}
});

Template.index.helpers({
	'username': function(){
			return Meteor.user().profile.name;
		},
	'image': function(){
			return getImage("brick wall");
		}
});
