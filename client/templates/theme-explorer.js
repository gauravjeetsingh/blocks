Template.themeCollection.events({
	'click .theme': function(e){
		if(e.target.className == "big-letter"){
			theme = $(e.target).next('.themename').text();
		} else if(e.target.className == "themename"){
			theme = $(e.target).text();
		}else{
			theme = $(e.target).children()[1].textContent;
		}
		Router.go('/create/'+theme);
	},
});

Template.themeCollection.onRendered(function(){
	$('.editor-title').text("Choose a theme:");
	$('.editor-title').attr('contenteditable', 'false')
	Meteor.call('getThemes', function(err, data){
				  themeArray = data;
				  for(var i in themeArray){
					$('#theme-selector').append("<div class=\"theme\">"+
								   "<p class=\"big-letter\">"+
								    themeArray[i].charAt(0) +"</p>"+
								   "<p class=\"themename\">"+
									themeArray[i]+"</p></div>");
				  }
			});
});

