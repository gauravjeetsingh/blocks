Router.route('/',{
	template: 'index',
});

Router.route('/create', {
	layoutTemplate: 'editor',
	template: 'themeCollection',
	onBeforeAction: function(){
		var currentUser = Meteor.userId();
		if(currentUser){
			this.next();
		} else{
			this.render("index");
		}
	}
});

Router.route('/create/:theme', {
	layoutTemplate: 'editor',
	template: 'form',
	data: function(){
		themeSelected = this.params.theme;
		fetchLayouts(themeSelected);
	},
	onBeforeAction: function(){
		var currentUser = Meteor.userId();
		if(currentUser){
			this.next();
		} else{
			this.render("index");
		}
	}
});

Router.route('/view', {
	template: 'myPresentations'
});

Router.route('/view/:user/:title', {
	template: 'presentationShowcase',
	data: function(){
		var user = this.params.user;
		var title = this.params.title;
		result =  PresentationList.findOne({
				userId: user,
				presentationTitle: title
			});
		viewPresentation(result);
	}
});
