shell = Meteor.npmRequire('shelljs');
//h2p = Meteor.npmRequire('html-to-pdf');

pw = shell.pwd();
pw2 = pw.split('.meteor');
var themeName;

Meteor.methods({
	getThemes: function(){
		list = shell.ls(pw2[0] + '/public/themes/');
		return list;
	},

	getLayouts: function(theme){
		themeName = theme;
		list = shell.ls( pw2[0] + 'public/themes/'+ themeName +'/templates/');
		for(var i in list){
			var name = list[i].split('.html');
			list[i] = name[0];
		}
		return list;
	},

	getTemplate: function(layout){
		var layoutTemplate = shell.cat(pw2[0] + 'public/themes/'+themeName+'/templates/'+
						layout+'.html');
		return layoutTemplate;
	},

	saveFile: function(jsonString, name){
		shell.echo(jsonString).to(pw2[0] + '/public/json/'+name+'.json');
	}/*,

	html2pdf: function(html){
		h2p.convertHTMLString(html, pw2[0]+'/public/try.pdf',
			function(error, success){
			if(error){
				console.log("Oh sheeet! errorz");
				console.log(error);
			} else {
				console.log("Woot! Woot! success");
				console.log(success);
			}
		});
	}*/
});
