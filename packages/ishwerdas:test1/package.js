Package.on_use(function(api){
	api.use("templating", "client");

	api.add_files([ 'handlebars-v3.0.3.js',
			'jquery-ui.js',
			'jquery.min.js',
			'get-image.js',
			'layout.html',
			'render.js'
			], 'client');

	api.export([	'getTemplateVariables',
			'goToPreviousSlide',
			'goToNextSlide',
			'generateJSON',
			'renderJSON',
			'goToSlide',
			'accordion',
			'resizable',
			'flickrAPI',
			'sortable',
			'getImage',
			'getJSON',
			'hb'
		   ], 'client');
});
