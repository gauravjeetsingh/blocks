getJSON = function(jsonURL){
	var jsonData="";
	slides = [];
	$.ajax({
	   url: jsonURL,
	   dataType: "json",
	   mimeType: "application/json",
	   async: false,
	   success: function(data){
			jsonData = data;
		    },
	   error: function(e){
			alert('There might be some problem with your '+jsonURL+' file');
			console.log(e);
		 }
	});
	return jsonData;
}


renderJSON = function(jsonObject, theme){
	var slideData = jsonObject;
	themeName = theme;
	count = 0;
	document.getElementById('presentation-slides').innerHTML = "";
	for(var i in slideData.slides){
		count++;
		var layout = slideData.slides[i].layout;

		xyz = $.ajax({
			url: '/themes/'+ theme  +'/templates/' +layout+'.html', 
			async: false, 
			error: function(e){
				alert('Unable to create layout '+layout ); 
				}
			}).responseText;

		source = xyz;
		template = hb.compile(source);
		/*if(slideData.slides[i].bgImage){
			var imageOptions = getImage(slideData.slides[i].bgImage);
			slideData.slides[i].bgImage = imageOptions[0];
		}*/

		context = slideData.slides[i];
		output = template(context);

		document.getElementById('presentation-slides').innerHTML += output;
		$('head').append('<link rel="stylesheet" href="/themes/'
				+theme+'/stylesheets/style.css" type="text/css" />');
	}
	var fullWidth = 100 * count;
	fullWidth = fullWidth + "%";
	$('#presentation-slides').css('width', fullWidth);
	var slideWidth = 100 / count;
	slideWidth = slideWidth + '%';
	$('.slide').css('width', slideWidth);
}

getTemplateVariables = function(data){
	var regex = /\{\{\s*(\w+)\s*\}\}/g;
	matched = data.match(regex);
	var tags = [];
	for( var i = 0; i < matched.length; i++){
		if (matched[i] !== "{{else}}" && matched[i] !== "{{this}}"){
			tags[i] = matched[i].replace("{{","").replace("}}","");
		}
	}
	return tags;
}


var currentPosition = 0;

goToPreviousSlide = function(){
	if(currentPosition != 0){
		currentPosition = currentPosition + 100;
		$('#presentation-slides').css('left', currentPosition+'%');
	}
}

goToNextSlide = function(){
	if(currentPosition > -(count-1)*100){
		currentPosition = currentPosition - 100;
		$('#presentation-slides').css('left', currentPosition+'%');
	}
}

goToSlide = function(n){
	currentPosition = -(n)*100;
	$('#presentation-slides').css('left', currentPosition+'%');
}
