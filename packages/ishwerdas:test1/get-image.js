flickrAPI = function(tag){
	tagWords = tag.split(' ');
	text = tagWords[0];
	for(i=1; i<tagWords.length; i++){
		text += "+" + tagWords[i];
	}
	console.log(text);
	url = "";
	apiurl = "https://api.flickr.com/services/rest/?" +
		"method=flickr.photos.search" +
		"&api_key=1bfbc7ca5b958dfcf49ad01ef1574356" +
		"&text="+ text +
		"&license=2%2C3%2C4%2C5%2C6%2C9" +
		"&sort=relevance" +
		"&per_page=1" +
		"&format=json" +
		"&nojsoncallback=1";
	console.log(apiurl); 
	data = $.ajax({ url: apiurl, async: false});
	src = data.responseJSON.photos.photo;
	var f_url = [];
	for(var i = 0; i < src.length; i++){
		f_url.push("https://farm" + 
			src[i].farm +
			".staticflickr.com/" + 
			src[i].server+"/" +
			src[i].id + "_" + 
			src[i].secret + "_b.jpg");
		console.log(f_url);
	}
	return f_url;
}

getImage = function(tag){
	if(tag){
		searchVar = false;
		var localURL = localSearch(tag);
		var url = [];
		if(searchVar){
			url = localURL;
		} else {
			url = flickrAPI(tag);
		}
		return url;
	}
}

localSearch = function(tag){
	var path;

	var imageObj = getJSON("/images/meta.json");
	var localPath = [];
	for (var i in imageObj.images){
		var tagString = imageObj.images[i].tags;
		tagArray = tagString.split(',');

		for( var j=0; j < tagArray.length; j++){
			if( tag === tagArray[j]){
				path = imageObj.images[i].path;
				searchVar = true;
				localPath.push(path);
			}
		}
	
	}
	return localPath;
}
